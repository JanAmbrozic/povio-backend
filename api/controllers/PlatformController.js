/**
 * PlatformController
 *
 */

module.exports = {

  _config: {
    actions: true,
    shortcuts: false,
    rest: false
  },

  // List users in a most liked to least liked
  'mostLiked': async function(req, res) {
    try {
      var users = await User.find().sort([{ likedByCount: 'DESC' }]);
      return res.json(users);
    } catch (err) {
      sails.log.error(err);
      return res.serverError(err);
    }
  },
};

