/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const bcrypt = require('bcrypt');

module.exports = {

  _config: {
    actions: true,
    shortcuts: false,
    rest: false
  },

  // Sign up to the system (username, password)
  'signup': function(req, res) {
    User.create(req.body).exec((err, user) => {
      if (err) {
        return res.serverError('Something went wrong');
      }
      return res.json(user);
    });
  },

  // Logs in an existing user with a password
  'login': async function(req, res) {
    // Return error if username or password are not in the parameters
    if (!req.body.username || !req.body.password) {
      return res.badRequest({
        err: 'username or password cannot be empty'
      });
    }

    try {
      // Find the user from username
      let user = await User.findOne({
        username: req.body.username
      });

      // if user is not found don't do anything, if it is encrypt the password
      if (!user) {
        return res.notFound({err: 'Could not find username,' + req.body.username + ' sorry.'});
      } else {
        bcrypt.compare(req.body.password, user.encryptedPassword, (err, result) => {
          if (result) {
            //password is a match
            return res.json({
              user: user,
              token: jwToken.sign(user)//generate the token and send it in the response
            });
          } else if (err) {
            //password is not a match
            return res.forbidden({ err: 'username and password combination do not match' });
          }
        });
      }

    } catch (err) {
      if (err) {
        return res.serverError(err);
      }
    }

  },

  // Get the currently logged in user information
  'profile': function(req, res) {
    User.findOne({
      id: req.user.data.id
    }).exec((err, user) => {
      if (err) {
        return res.serverError(err);
      }
      return res.json(user);
    });
  },

  // Update the current user's password
  'updatePassword': async function(req, res) {
    if (! req.body.password || ! req.body.confirmation ||  req.body.password !== req.body.confirmation) {
      return  res.serverError({ err: ['Password does not match confirmation'] });
    }

    req.user.password = req.body.password;
    req.user.confirmation = req.body.confirmation;

    passwordHandler.hash(req.user, async () => {
      var newUser = await User.update({id: req.user.data.id}, {encryptedPassword: req.user.encryptedPassword}).fetch();
      sails.log(newUser);
      return res.json(newUser);
    });
  },

  // List username & number of likes of a user
  'public': async function(req, res) {

    try {
      let user = await User.findOne({id: req.param('id')}).populate('likedUsers');
      if (user) {
        let returnData = {
          username: user.username,
          numberOfLikedUsers: user.likedUsers.length
        };
        return res.json(returnData);
      } else {
        return  res.notFound({err: `Could not find user with id: ${req.param('id')}`});
      }
    } catch (err) {
      if (err) {
        return res.serverError('Something went wrong');
      }
    }
  },

  // Like a user
  'like': async function(req, res) {
    try {
      sails.log(' req.user.data.likedByCount',  req.user.data.likedByCount);
      await User.addToCollection( req.user.data.id, 'likedUsers').members([req.param('id')]);
      await User.addToCollection(req.param('id'), 'likedBy').members([req.user.data.id]);

      // if (isNaN(req.user.data.likedByCount)) {
      //     await User.update({id: req.user.data.id}, {likedByCount: 0});
      // } else {
      let likedUser = await User.findOne({id: req.param('id')});
      let newCount = likedUser.likedByCount + 1;
      await User.update({id: req.param('id')}, {likedByCount: newCount});
      //}

    } catch (err) {
      sails.log.error(err);
      return res.serverError(err);
    }
    return res.ok();
  },

  // Unlike a user
  'unlike': async function(req, res) {
    try {
      await User.removeFromCollection( req.user.data.id, 'likedUsers').members([req.param('id')]);
      await User.removeFromCollection(req.param('id'), 'likedBy').members([req.user.data.id]);
    } catch (err) {
      sails.log.error(err);
      return res.serverError(err);
    }
    return res.ok();
  },

  // TODO: remove this, just for quick and dirty testing
  'all': async function(req, res) {
    let users = await User.find();
    return res.json(users);
  },
};

