/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  /* Lifecycle Callbacks */
  beforeCreate: function (values, cb) {
    if (!values.password || !values.confirmation || values.password !== values.confirmation) {
      return cb({ err: ['Password does not match confirmation'] });
    }
    passwordHandler.hash(values, cb);
  },

  /* custom stringification which removes the encryptedPassword from the object' */
  customToJSON: function() {
    return _.omit(this, ['encryptedPassword']);
  },

  attributes: {
    'username': {
      type: 'string',
      unique: true,
      required: true
    },
    'email': {
      type: 'string',
      unique: true,
      required: true
    },
    'firstname': {
      type: 'string'
    },
    'lastname': {
      type: 'string'
    },
    'encryptedPassword': {
      type: 'string'
    },
    'likedUsers': {
      collection: 'user'
    },
    'likedBy': {
      collection: 'user'
    },
    'likedByCount' : {
      type: 'number'
    }
  }

};

