const bcrypt = require('bcrypt');

module.exports = {
  'hash': function (values, cb) {
    // Hash password
    bcrypt.hash(values.password, 10, (err, hash) => {
      if (err) {return cb(err);}
      values.encryptedPassword = hash;

      //Delete the passwords so that they are not stored in the DB
      delete values.password;
      delete values.confirmation;

      //calling cb() with an argument returns an error. Useful for canceling the entire operation if some criteria fails.
      cb();
    });
  }
};
