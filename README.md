# povio-backend

## TODO - missing features
* finish refactoring UserController to use async/await everywhere
* actually check if user already liked a user before incrementing a count of likes 
* add unit tests for all the endpoints

## Instalation
* Use stable node.js 8.x.x version and run `npm install`

## Running 
* Run `sails lift`
* For now this is using local store but if you want to use mongoDB just edit `config/datastores.js` and User model as described here: https://sailsjs.com/documentation/tutorials/using-mongo-db

## Testing 
* Run `npm run test`