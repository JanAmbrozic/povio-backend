// test/integration/controllers/PlatformController.test.js
const supertest = require('supertest');
const assert = require('assert');

/***************************************************************************
*
* /most-liked
*   List users in a most liked to least liked
*
*****************************************************************************
* */
describe('PlatformController.mostLiked', () => {

  describe('/most-liked', () => {
    it('should return a list of users',  (done) => {
      supertest(sails.hooks.http.app)
      .get('/most-liked')
      .expect(200)
      .then(response => {
        if(response.body) {
          assert(response.body.length > 0, true);
          if(response.body.length >= 2) {
            assert(response.body[0].likedByCount > response.body[1].likedByCount, true);
          }
        }
        done();
      });
    });

    it('should return 404 because POST does not exist on this endpoint',  (done) => {
      supertest(sails.hooks.http.app)
        .post('/most-liked')
        .expect(404, done);
    });
  });

});
