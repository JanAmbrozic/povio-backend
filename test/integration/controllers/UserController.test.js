// test/integration/controllers/UserController.test.js
var supertest = require('supertest');

describe('UserController.public', () => {

  // List username & number of likes of a user
  describe('/user/:id', () => {
    it('should return a user',  (done) => {
      supertest(sails.hooks.http.app)
      .get('/user/1')
      .expect(200, done);
    });

    it('should not return a user because of wrong id',  (done) => {
      supertest(sails.hooks.http.app)
      .get('/user/-1')
      .expect(404, done);
    });

    it('should produce a 500 server error because of invalid criteria',  (done) => {
      supertest(sails.hooks.http.app)
      .get('/user/xy234x')
      .expect(500, done);
    });
  });

  // Sign up to the system (username, password)
  describe('/signup', () => {
    it('should create a new user',  (done) => {
      supertest(sails.hooks.http.app)
      .post('/signup')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({ username: 'testUserNew', password: '1234', confirmation: '1234', email: 'testUserNew@test.email' })
      .expect(200, done);
    });

    it('should not create a new user because of missing confirmation',  (done) => {
      supertest(sails.hooks.http.app)
      .post('/signup')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({ username: 'testUserNew2', password: '1234', email: 'testUserNew2@test.email' })
      .expect(500, done);
    });

    it('should not create a new user because of missing username',  (done) => {
      supertest(sails.hooks.http.app)
      .post('/signup')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({password: '1234', confirmation: '1234', email: 'testUserNew3@test.email' })
      .expect(500, done);
    });
  });

  // Logs in an existing user with a password
  describe('/login', () => {
    it('should return a bad request because of wrong content',  (done) => {
      supertest(sails.hooks.http.app)
      .post('/login')
      .expect(400, done);
    });

    it('should login an existing user',  (done) => {
      supertest(sails.hooks.http.app)
      .post('/login')
      .send({ username: 'testUserNew', password: '1234' })
      .expect(200, done);
    });

    it('should return a 404',  (done) => {
      supertest(sails.hooks.http.app)
      .post('/login')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({ username: '2143', password: '1234' })
      .expect(404, done);
    });
  });

  // Logs in an existing user with a password
  describe('/me', () => {
    // function createAuthenticatedRequest(done) {
    //   var authenticatedRequest = supertest.agent(sails.hooks.http.app);
    //   authenticatedRequest
    //     .post('/login')
    //     .send({ username: 'testUser1', password: '1234' })
    //       .end((error, _response) => {
    //         console.log(_response);
    //         if (error) {
    //           throw error;
    //         }
    //         done(authenticatedRequest);
    //       });
    // }

    // it('should return data about me',  (done) => {
    //   createAuthenticatedRequest((request) => {
    //     request
    //     .get('/me')
    //     .expect(200, done);
    //   });

    // });

    it('should refuse to return the data because we are not logged in',  (done) => {
      supertest(sails.hooks.http.app)
        .get('/me')
        .expect(401, done);
    });

  });

});
