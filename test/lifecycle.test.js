var sails = require('sails');

before( function(done) {
  // Increase the Mocha timeout so that Sails has enough time to lift, even if you have a bunch of assets.
  this.timeout(5000);

  sails.lift({
    models: { migrate: 'drop' }
  },async (err) => {
    if (err) { return done(err); }

    // create 2 test users
    await User.create({
      id: 1,
      username: 'testUser1',
      email: 'testUser1@test.email',
      firstname: 'first test',
      lastname: 'last test',
      password: '1234',
      confirmation: '1234'
    });

    await User.create({
      id:2,
      username: 'testUser2',
      email: 'testUser2@test.email',
      firstname: 'first test',
      lastname: 'last test',
      password: '12345',
      confirmation: '12345'
    });

    await User.addToCollection(1, 'likedUsers').members([2]);
    await User.addToCollection(2, 'likedBy').members([1]);
    await User.update({id: 1}, {likedByCount: 1});

    return done();
  });
});

after( (done) => {
  sails.lower(done);
});
