/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *
  *   'get /signup': { view: 'conversion/signup' },
  'post /signup': 'AuthController.processSignup',
  'get /login': { view: 'portal/login' },
  'post /login': 'AuthController.processLogin',
  '/logout': 'AuthController.logout',
  'get /me': 'UserController.profile'              *
  ***************************************************************************/

  '/': {
    view: 'pages/homepage'
  },

  'post /signup': 'UserController.signup',
  'post /login': 'UserController.login',
  'get  /me': 'UserController.profile',
  'put  /me/update-password': 'UserController.updatePassword',
  'get  /user/:id/': 'UserController.public',
  'get  /user/:id/like': 'UserController.like',
  'get  /user/:id/unlike': 'UserController.unlike',
  'get  /most-liked': 'PlatformController.mostLiked',
  'get  /all': 'UserController.all'

};
